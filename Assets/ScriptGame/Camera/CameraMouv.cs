﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMouv : MonoBehaviour
{
    public Transform player1, player2;
    public float zoomSize = 5;
    public float dist, maxSize , minSize;

    private void Start()
    {
        PlayerMovement[] monsterList = FindObjectsOfType<PlayerMovement>(); //trouver lesmonstres
        if (!player1)
        {
            player1 = monsterList[0].gameObject.transform;
        }

        if (!player2)
        {
            player2 = monsterList[1].gameObject.transform;
        }
    }

    void Update()
    {
        GetComponent<Camera>().orthographicSize = zoomSize;   //taille dee la camera 
        if (dist <= minSize)
        {
            zoomSize = minSize;      //ne va jamais en dessous de la distance minimum
        }
        if (dist >= maxSize)
        {
            zoomSize = maxSize;    //ne va jamais au dessus de la distance maximum
        }

        if (dist < maxSize && dist > minSize)
        {
            zoomSize = dist;      //si entre le minimum et le maximum de distance alors la taille s'adaptera a la distance
        }
        
        transform.position = new Vector3((player1.position.x + player2.position.x )/2 ,(player1.position.y + player2.position.y) /2,-10); //placement de la camera 
        dist = Vector3.Distance(player1.position, player2.position);   //trouver le milieu entre les deux joueurs

    }
}
