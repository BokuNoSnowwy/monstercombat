﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManagerSelectionObject : MonoBehaviour
{
    public int randomStart;
    public GameObject[] positionObjectChoice;
    public GameObject[] ObjectChoice;
    public GameObject choiceP1;
    public GameObject choiceP2;
    public GameObject[] positionChoice;
    public bool[] objectP1;
    public bool[] objectP2;
    public bool checkTimerStart;
    public string[] infosObject;
    public TMP_Text textInfos1;
    public TMP_Text textInfos2;
    public TMP_Text textTurnPlayer;
    public float timerNumber;
    public float timerNumberArrondi;

    private int p1;
    private int p2;

    public bool choiceTurn;

    public bool[] objectActif;

    public GameObject[] positionImagePlayer;
    public GameObject[] imagePlayerp1;
    public GameObject[] imagePlayerp2;
    public int heroP1;
    public int heroP2;
    public GameObject recupInfosSelection;
    public GameObject saveObject;
    // Start is called before the first frame update
    void Start()
    {
        saveObject = GameObject.Find("SaveObjectSceneArena");
        recupInfosSelection = GameObject.Find("Data");
        heroP1 = recupInfosSelection.GetComponent<Info_Sauvegarder>().p1;
        heroP2 = recupInfosSelection.GetComponent<Info_Sauvegarder>().p2;
        imagePlayerp1[heroP1].transform.position = positionImagePlayer[0].transform.position;
        imagePlayerp2[heroP2].transform.position = positionImagePlayer[1].transform.position;
        timerNumber = 5;
        randomStart = Random.Range(0, 2);// random number pour savoir qui commence
        Debug.Log(randomStart);
        if (randomStart == 1)//si number = 1 alors P2 commence
        {
            choiceTurn = true;//attention remettre en true lorsque c'est opérationnel !
        }
        choiceP1.transform.position = positionObjectChoice[p1].transform.position;
        choiceP2.transform.position = positionObjectChoice[p2].transform.position;
        
        for (int i = 0; i < 12; i++)//boucle for verrouille les object déjà sélectionner dans le round précédent
        {
            Debug.Log(i);
            if (saveObject.GetComponent<SaveObjectPlayer>().objectVerr[i] == true)
            {
                positionObjectChoice[i].gameObject.transform.GetComponent<Image>().color = Color.black;
                objectActif[i] = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (checkTimerStart == false)
        {
            if (choiceTurn == false)
            {
                // affichage des text sur la scene
                textTurnPlayer.text = "C'est au tour du Player 1";
                if (objectActif[p1])
                {
                    textInfos1.text = "<color=red>Cet objet n'est plus disponible : </color> \n" + infosObject[p1];
                    textInfos2.text = "<color=red>Cet objet n'est plus disponible : </color> \n" + infosObject[p1];
                }
                else
                {
                    textInfos1.text = "<color=green>Vous pouvez prendre cet objet : </color> \n" + infosObject[p1];
                    textInfos2.text = "<color=green>Vous pouvez prendre cet objet : </color> \n" + infosObject[p1];
                }
                if (Input.GetAxisRaw("Horizontal_P1") < 0)
                {
                    if (p1 >= 1)
                    {
                        if (Input.GetButtonDown("Horizontal_P1"))
                        {
                            p1--;
                        }
                    }
                }
                if (Input.GetAxisRaw("Horizontal_P1") > 0)
                {
                    if (Input.GetButtonDown("Horizontal_P1"))
                    {
                        p1++;
                    }
                }
                if (Input.GetButtonUp("ValidationP1"))
                {
                    if (objectActif[p1] == true)
                    {
                        Debug.Log("erreur selection");
                        //message d'erreur a mettre au millieu du screen
                    }
                    if (objectActif[p1] == false)
                    {
                        positionObjectChoice[p1].gameObject.transform.GetComponent<Image>().color = Color.black; //lorsque validation object passer gris
                        if (objectP1[0] == false)
                        {
                            ObjectChoice[p1].transform.position = positionChoice[0].transform.position;
                            objectP1[0] = true;
                            saveObject.GetComponent<SaveObjectPlayer>().FirstChoiceP1 = p1;
                            saveObject.GetComponent<SaveObjectPlayer>().objectVerr[p1] = true;
                        }
                        else if (objectP1[1] == false)
                        {
                            ObjectChoice[p1].transform.position = positionChoice[1].transform.position;//deplacement object au niveau des object selectionner par le player
                            objectP1[1] = true;
                            saveObject.GetComponent<SaveObjectPlayer>().SecondChoiceP1 = p1;
                            saveObject.GetComponent<SaveObjectPlayer>().objectVerr[p1] = true;
                        }
                        choiceTurn = true;
                        objectActif[p1] = true;
                    }
                }
            }
            if (choiceTurn == true)
            {
                // affichage des text sur la scene
                textTurnPlayer.text = "C'est au tour du Player 2";
                if (objectActif[p2])
                {
                    textInfos1.text = "<color=red>Cet objet n'est plus disponible : </color> \n " + infosObject[p2];
                    textInfos2.text = "<color=red>Cet objet n'est plus disponible : </color> \n " + infosObject[p2];
                }
                else
                {
                    textInfos1.text = "<color=green>Vous pouvez prendre cet objet : </color> \n" + infosObject[p2];
                    textInfos2.text = "<color=green>Vous pouvez prendre cet objet : </color> \n" + infosObject[p2];
                }
                if (Input.GetAxisRaw("Horizontal_P2") < 0)
                {
                    if (p2 >= 1)
                    {
                        if (Input.GetButtonDown("Horizontal_P2"))
                        {
                            p2--;//soustraction de 1 
                        }
                    }
                }
                if (Input.GetAxisRaw("Horizontal_P2") > 0)
                {
                    if (Input.GetButtonDown("Horizontal_P2"))
                    {
                        p2++;//incrémentation de 1 
                    }
                }
                if (Input.GetButtonUp("ValidationP2"))
                {
                    if (objectActif[p2] == true)
                    {
                        Debug.Log("erreur selection");
                        //message d'erreur a mettre au millieu du screen
                    }
                    if (objectActif[p2] == false)
                    {
                        positionObjectChoice[p2].gameObject.transform.GetComponent<Image>().color = Color.black;//lorsque validation object passer gris
                        if (objectP2[0] == false)
                        {
                            ObjectChoice[p2].transform.position = positionChoice[2].transform.position;
                            objectP2[0] = true;
                            saveObject.GetComponent<SaveObjectPlayer>().FirstChoiceP2 = p2;
                            saveObject.GetComponent<SaveObjectPlayer>().objectVerr[p2] = true;
                        }
                        else if (objectP2[1] == false)
                        {
                            ObjectChoice[p2].transform.position = positionChoice[3].transform.position;//deplacement object au niveau des object selectionner par le player
                            objectP2[1] = true;
                            saveObject.GetComponent<SaveObjectPlayer>().SecondChoiceP2 = p2;
                            saveObject.GetComponent<SaveObjectPlayer>().objectVerr[p2] = true;
                        }
                        choiceTurn = false;
                        objectActif[p2] = true;
                    }
                }
            }
            //remise a zero du int si 12 ou +
            if (p1 == 12)
            {
                p1 = 0;
            }
            if (p2 == 12)
            {
                p2 = 0;
            }
            //position carré selection des player
            choiceP1.transform.position = positionObjectChoice[p1].transform.position;
            choiceP2.transform.position = positionObjectChoice[p2].transform.position;
            if (objectP1[0] && objectP1[1] && objectP2[0] && objectP2[1])
            {
                checkTimerStart = true;
            }
        }

        if (checkTimerStart == true)
        {
            Time.timeScale = 1;
            Debug.Log("debut décompte");
            //timer perte valeur dans le temps
            timerNumber -= Time.deltaTime;
            timerNumberArrondi = Mathf.RoundToInt(timerNumber);// on arrondis la valeur du float comment si c'était un int
            // affichage des text sur la scene
            textTurnPlayer.text = "La partie va commencer dans : "+ timerNumberArrondi;
            
            // changement scene quand timer = 0
            if (timerNumber <= 0)
            {
                SceneManager.LoadScene(4);
            }
        }
    }
}
