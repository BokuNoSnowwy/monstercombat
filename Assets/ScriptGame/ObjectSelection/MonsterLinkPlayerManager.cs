﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MonsterLinkPlayerManager : MonoBehaviour
{
    public GameObject[] monstersPrefabs;
    public GameObject joueur1, joueur2;
    public Transform[] spawnPoints;
    public int scoreJ1, scoreJ2;
    public float compteur;
    public static bool comptage , autorisation = true , victoryCouldown;
    public GameObject monstre1;
    public GameObject monstre2; 
    public GameObject menu;
    public Info_Sauvegarder monsterChoice;
    public TextMeshProUGUI textVictory, textCompteur, textScoreJ1, textScoreJ2, texteScoreFinalJ1,textScoreFinalJ2;

    private void Awake()
    {
        if (monsterChoice = FindObjectOfType<Info_Sauvegarder>()) //instanciate les monstres choisient 
        {
            InitialiseFight(monsterChoice.p1, monsterChoice.p2);
        }
        autorisation = true;
        compteur = 3f;
    }

    void Update()
    {
        textCompteur.text = Mathf.RoundToInt(compteur).ToString(); //calcul le couldown affiche
        joueur1 = monstre1;
        joueur2 = monstre2; //pour recuperer les joueurs choisit
        if (!joueur1.activeSelf && autorisation || !joueur2.activeSelf && autorisation)//si un des joueurs meurt alors tout le systeme de gamesetup fonctionne
        {
            comptage = true;
            autorisation = false;
            StartCoroutine(GameSetUp());
        }
          if (comptage) 
          {
              if (victoryCouldown) //démarre le cooldown
              {
                  compteur -= Time.deltaTime;
                  if (compteur <= 0) //si 0 il se renitialise
                  {
                      compteur = 0f;
                      comptage = false;
                  }
              }
          }
          if (scoreJ1 == 1 || scoreJ2 == 1) //si un des deux joueurs atteint 1 alors il a gagner
          {
              if (scoreJ1 == 1)
              {
                  SaveObjectPlayer.Instance.scoreJ1Final++;
                  texteScoreFinalJ1.text =SaveObjectPlayer.Instance.scoreJ1Final.ToString();
                  scoreJ1 = 0;
              }
              else
              {
                  SaveObjectPlayer.Instance.scoreJ2Final++;
                  textScoreFinalJ2.text = SaveObjectPlayer.Instance.scoreJ2Final.ToString();
                  scoreJ2 = 0;
              }
              menu.SetActive(true);
          }
          if (SaveObjectPlayer.Instance.scoreJ1Final == 2 || SaveObjectPlayer.Instance.scoreJ2Final == 2) //si un des deux joueurs atteint 1 alors il a gagner
          {
             Application.Quit();
          }
    }

    public void InitialiseFight(int monsterPlayer1, int monsterPlayer2) 
    {
        //TODO Wait for 3 seconds
        GameObject monster1 =
            Instantiate(monstersPrefabs[monsterPlayer1], spawnPoints[0].position, Quaternion.identity);
        GameObject monster2 =
            Instantiate(monstersPrefabs[monsterPlayer2], spawnPoints[1].position, Quaternion.identity);


        monstre1 = monster1;
        monstre2 = monster2;
        monster1.GetComponent<PlayerMovement>().player = "P1";
        monster2.GetComponent<PlayerMovement>().player = "P2";

        monster2.GetComponent<Transform>().rotation = new Quaternion(0, -180f, 0, 0);

        monster1.layer = 11;
        monster1.GetComponent<PlayerAttack>().enemyLayers.value = LayerMask.GetMask("PlayerLayer2");

        monster2.layer = 12;
        monster2.GetComponent<PlayerAttack>().enemyLayers = LayerMask.GetMask("PlayerLayer1");

    }
    IEnumerator GameSetUp()
    {
        textVictory.gameObject.SetActive(true);
        if (!joueur1.activeSelf)
        {
            scoreJ2++;
            textScoreJ2.text = scoreJ2.ToString(); //affiche le score en fonction de qui meurt
            textVictory.text = "Player 2 Win";
        }
        if (!joueur2.activeSelf)
        {
            scoreJ1++;
            textScoreJ1.text = scoreJ1.ToString();
            textVictory.text = "Player 1 Win";
        }
        victoryCouldown = true;
        yield return new WaitForSeconds(3f); //apres 3seconde
        victoryCouldown = false;
        compteur = 3f;
        textVictory.gameObject.SetActive(false);
        monstre1.GetComponent<CombatStat>().respawn(); //les ennemy respawn a une position precise
        monstre2.GetComponent<CombatStat>().respawn();
        monstre1.transform.position = spawnPoints[0].transform.position;
        monstre2.transform.position = spawnPoints[1].transform.position;
        joueur1.SetActive(true); //ils revivent
        joueur2.SetActive(true);
        autorisation = true;
    }

}


