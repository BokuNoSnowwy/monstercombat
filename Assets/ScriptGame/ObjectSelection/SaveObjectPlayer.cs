﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveObjectPlayer : MonoBehaviour
{
    public int FirstChoiceP1;
    public int FirstChoiceP2;
    public int SecondChoiceP2;
    public int SecondChoiceP1;

    public static SaveObjectPlayer Instance;
    
    public int scoreJ1Final, scoreJ2Final;//score sauvegarder 

    public bool[] objectVerr;

    private void Awake()
    {
        if (Instance == null) //singleton pour trouver l'objet
        {
            Instance = this;
        }
        else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
