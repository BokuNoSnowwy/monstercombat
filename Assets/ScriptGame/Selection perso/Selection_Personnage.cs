﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;


public class Selection_Personnage : MonoBehaviour
{
    public GameObject dataSave; // l'objet dans lequel les info sont conserver et passer dans la scène suivante

    public GameObject[] pos = new GameObject[4]; // liste d'objet qui permet de ce déplacer entre les case

    public GameObject statP1; //prefab du monstre 1
    public GameObject statP2; // prefab du monstre 2
    public GameObject statP3;
    public GameObject statP4;
    
    public TMP_Text text;
    public TMP_Text p1Display;
    public TMP_Text p2Display;

    private CombatStat stat1; //variable privé a qui on assigne les valeurs du monstre 1
    private CombatStat stat2; //variable privé a qui on assigne les valeurs du monstre 2
    private CombatStat stat3; //variable privé a qui on assigne les valeurs du monstre 3
    private CombatStat stat4; //variable privé a qui on assigne les valeurs du monstre 4

    private bool player1Choose; // Si le joueur 1 a choisi
    private bool player2Choose; // Si le joueur 2 a choisi

    private int p1 = 0; // position du joueur 1
    private int p2 = 0; // position du joueur 2
    
    private float time = 3; //float utiliser pour le timer choix

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(p1);
        Debug.Log(p2);

        stat1 = statP1.GetComponent<CombatStat>();
        stat2 = statP2.GetComponent<CombatStat>();
        stat3 = statP3.GetComponent<CombatStat>();
        stat4 = statP4.GetComponent<CombatStat>();
        
        pos[0].GetComponent<SpriteRenderer>().color = Color.red;
        pos[0].GetComponent<SpriteRenderer>().color = Color.blue;
    }

    // Update is called once per frame
    void Update()
    {
        Colors();
        if (player1Choose is false)
        {
            Player1Input();
        }

        if (player2Choose is false)
        {
            Player2Input();
        }

        StatDisplay();
        Choice();
    }

    void Choice() //Permet de vérifier si les deux joueur on choisi
    {
        if (Input.GetButtonDown("ValidationP1"))
        {
            player1Choose = true;
            dataSave.GetComponent<Info_Sauvegarder>().p1 = p1;
        }
        else if (Input.GetButtonDown("ValidationP2"))
        {
            player2Choose = true;
            dataSave.GetComponent<Info_Sauvegarder>().p2 = p2;
        }

        if (player1Choose && player2Choose) //passe a la scène suivante quand les deux joueur on choisi et affiche un timer
        {
            DontDestroyOnLoad(dataSave);

            time = time - Time.deltaTime;

            text.text = time.ToString("F0"); //affiche le timer sans decimal

            if (time < 0) //passe a la scène suivante
            {
                SceneManager.LoadScene(1);
            }

            DontDestroyOnLoad(dataSave);
        }
    }

    void Colors()//Change la couleur des case ou ce trouve p1 et p2 (applique un mélange si elle sont sur la même)
    {
        if (p1 == p2)
        {
            pos[p1].GetComponent<SpriteRenderer>().color = (Color.red + Color.blue);
        }
        else
        {
            pos[p1].GetComponent<SpriteRenderer>().color = (Color.red);
            pos[p2].GetComponent<SpriteRenderer>().color = (Color.blue);
        }
    }

    void Player1Input()//Reçois les input du joueurs 1 et change la couleur des case (mets en blanc la position précédente)
    {
        if (Input.GetAxis("Horizontal_P1") > 0.0f && p1 + 1 < pos.Length)
        {
            if (Input.GetButtonDown("Horizontal_P1"))
            {
                if (p1 != p2)
                {
                    pos[p1].GetComponent<SpriteRenderer>().color = Color.white;
                }

                pos[p1 + 1].GetComponent<SpriteRenderer>().color = Color.red;
                p1++;
                Debug.Log(p1);
            }
        }

        if (Input.GetAxis("Horizontal_P1") < 0.0f && p1 - 1 >= 0)
        {
            if (Input.GetButtonDown("Horizontal_P1"))
            {
                if (p1 != p2)
                {
                    pos[p1].GetComponent<SpriteRenderer>().color = Color.white;
                }

                pos[p1].GetComponent<SpriteRenderer>().color = Color.white;
                pos[p1 - 1].GetComponent<SpriteRenderer>().color = Color.red;
                p1--;
                Debug.Log(p1);
            }
        }
    }

    void Player2Input()//Reçois les input du joueurs 2 et change la couleur des case (mets en blanc la position précédente)
    {
        if (Input.GetAxis("Horizontal_P2") > 0.0f && p2 + 1 < pos.Length)
        {
            if (Input.GetButtonDown("Horizontal_P2"))
            {
                if (p2 != p1)
                {
                    pos[p2].GetComponent<SpriteRenderer>().color = Color.white;
                }

                pos[p2 + 1].GetComponent<SpriteRenderer>().color = Color.blue;
                p2++;
                Debug.Log(p2);
                
            }
        }

        if (Input.GetAxis("Horizontal_P2") < 0.0f && p2 - 1 >= 0)
        {
            if (Input.GetButtonDown("Horizontal_P2"))
            {
                if (p2 != p1)
                {
                    pos[p2].GetComponent<SpriteRenderer>().color = Color.white;
                }


                pos[p2 - 1].GetComponent<SpriteRenderer>().color = Color.blue;
                p2--;
                Debug.Log(p2);
            }
        }
    }

    void StatDisplay()//gère l'affichage des stat sur les bord de l'ecran
    {
        //affichage des stats du monstre 1
        if (p1 == 0)
        {
            p1Display.text = "P1 \nHealth : " + stat1.health + "\nAttack : " + stat1.attack + "\nDefense : " + stat1.defense;
        }

        if (p2 == 0)
        {
            p2Display.text = "P2 \nHealth : " + stat1.health + "\nAttack : " + stat1.attack + "\nDefense : " + stat1.defense;
        }
      //affichage des stats du monstre 2
        if (p1 == 1)
        {
            p1Display.text = "P1 \nHealth : " + stat2.health + "\nAttack : " + stat2.attack + "\nDefense : " + stat2.defense;
        }

        if (p2 == 1)
        {
            p2Display.text = "P2 \nHealth : " + stat2.health + "\nAttack : " + stat2.attack + "\nDefense : " + stat2.defense;
        }
        //affichage des stats du monstre 3
        if (p1 == 2)
        {
            p1Display.text = "P1 \nHealth : " + stat3.health + "\nAttack : " + stat3.attack + "\nDefense : " + stat3.defense + "\nPoison" + stat3.powerPoisonPlayer;
        }

        if (p2 == 2)
        {
            p2Display.text = "P2 \nHealth : " + stat3.health + "\nAttack : " + stat3.attack + "\nDefense : " + stat3.defense + "\nPoison " + stat3.powerPoisonPlayer;
        }
        //affichage des stats du monstre 4
        if (p1 == 3)
        {
            p1Display.text = "P1 \nHealth : " + stat4.health + "\nAttack : " + stat4.attack + "\nDefense : " + stat4.defense + "\nSaignement " + stat4.powerBloodPlayer ;
        }

        if (p2 == 3)
        {
            p2Display.text = "P2 \nHealth : " + stat4.health + "\nAttack : " + stat4.attack + "\nDefense : " + stat4.defense + "\nSaignement " + stat4.powerBloodPlayer;
        }
    }
}