﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : MonoBehaviour
{

    
    public bool guard; //boolean qui permet de savoir si le joueur garde ou non

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        GuardUp();
        Dash();
        Color();
    }

    void GuardUp()
    {
        if (GetComponent<PlayerMovement>().isDashing == false) // check si le joueur est en dash 
        {
            if (Input.GetButtonDown("Guard_" + GetComponent<PlayerMovement>().player)) // check si le joueur appuie sur le boutton de garde
            {
                Debug.Log("GuardUp");
               
                guard = true;
            }
            
            if (Input.GetButtonUp("Guard_" + GetComponent<PlayerMovement>().player)) // check si le joueur n'appuie plus sur le bouton de garde
            {
                Debug.Log("GuardDown");
                
                guard = false;
            }
        }
    }

    void Dash()
    {
        if (GetComponent<PlayerMovement>().isDashing ) //check si le joueur dash auquelle cas il n'est plus en garde et devra réappuyer sur le boutton pour la remettre
        {
            guard = false;
            
        }
    }

    void Color()
    {
        if (guard) //si le joueur est en garde il passe en bleu et repasse dans sa couleur d'origine une fois la garde enlever
        {
            GetComponent<SpriteRenderer>().color = UnityEngine.Color.blue;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = UnityEngine.Color.white;
        }
    }
}