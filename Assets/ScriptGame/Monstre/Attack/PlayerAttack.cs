﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerAttack : MonoBehaviour
{
    public bool canAttack;
    public PlayerMovement playerMoves;
    public CombatStat playerStats;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;

    public Transform[] hitPoints = new Transform[3];

    //Index de où se trouve le combo dans la liste
    public int indexCombo;

    public int nbOfCombo;
    public bool onCombo;
    public float comboTimer = 0.8f;
    public float resetComboTimer = 0.8f;


    //Bouge durant un combo
    public bool onLerp;
    private Vector3 nextMove;
    private float speedMove = 5f;
    public float timerOnLerp;
    public float resetTimerOnLerp = 0.5f;


    //Permet d'empecher le joueur de trop attaquer 
    public float timeBeforeAttacking;


    public MoveVectorForAttackInput[] listOfMovementEachAttack;

    [System.Serializable]
    public class MoveVector
    {
        public float x;
        public float y;
        public float z;
    }

    [System.Serializable]
    public class MoveVectorForAttackInput
    {
        public MoveVector[] listOfMoveVector;
    }

    // Start is called before the first frame update
    void Start()
    {
        playerStats = GetComponent<CombatStat>();
        playerMoves = GetComponent<PlayerMovement>();
        canAttack = true;
    }

    // Update is called once per frame
    void Update()
    {
        //Si le joueur rentre dans un combo, il y a un delais avant que le combo se reset, si il y a un coup porté durant ce combo, le reset
        if (onCombo)
        {
            comboTimer -= Time.deltaTime;
            if (comboTimer <= 0)
            {
                comboTimer = resetComboTimer;
                onCombo = false;
                nbOfCombo = 0;
                canAttack = true;
            }
        }

        //Empeche le joueur d'attaquer pendant un coup
        if (timeBeforeAttacking >= 0)
        {
            timeBeforeAttacking -= Time.deltaTime;
            canAttack = false;
        }
        else
        {
            canAttack = true;
        }

        //Fait avancer le joueur vers la position nextMove
        if (onLerp)
        {
            timerOnLerp -= Time.deltaTime;
            Vector3 pos1 = transform.position;
            //transform.position = Vector3.Lerp (pos1, nextMove, (Mathf.Sin(speedMove * Time.time) + 1.0f));
            transform.position = Vector3.Lerp(pos1, nextMove, Mathf.PingPong(Time.time * speedMove, 1.0f));
            if (timerOnLerp <= 0)
            {
                onLerp = false;
            }
        }
        

        if (timeBeforeAttacking <= 0)
        {
            //Si le joueur peut attaquer, si il appuit sur E, il lance la fonction Attack, cette dernière varie en fonction
            //de si il appuit sur le bouton Haut, bas ou pas de variation
            if (canAttack && playerMoves.knockbackCount <= 0)
            {
                if (playerMoves.moveVertical > 0)
                {
                    if (Input.GetButtonDown("Attack_" + playerMoves.player))
                    {
                        Attack(hitPoints[0], 0);
                    }
                }
                else if (playerMoves.moveVertical < 0)
                {
                    if (Input.GetButtonDown("Attack_" + playerMoves.player))
                    {
                        Attack(hitPoints[2], 2);
                    }
                }
                else if (playerMoves.moveVertical == 0)
                {
                    if (Input.GetButtonDown("Attack_" + playerMoves.player))
                    {
                        Attack(hitPoints[1], 1);
                    }
                }
            }
        }
    }

    //Fonction permettant de reset le combo
    public void ResetComboTimer()
    {
        if (onCombo)
        {
            comboTimer = 0;
        }
    }

    //En fonction du parametre (Haut, milieu, bas), créer un cercle de detection devant le joueur 
    public void Attack(Transform transform, int index)
    {
        //Si le joueur attaque, il rentre en combo
        if (!onCombo)
        {
            onCombo = true;
        }
        else
        {
            nbOfCombo += 1;
            comboTimer = resetComboTimer;
        }

        if (nbOfCombo >= 3)
        {
            nbOfCombo = 0;
        }

        //Donne un coup spécifique par rapport a l'emplacement de l'attaque et au niveau du combo
        switch (index)
        {
            case 0:
                switch (nbOfCombo)
                {
                    case 0:
                        //Coups en l'air
                        Debug.Log("Take Damage Up");
                        InflictDmgAndKnockbackv2(transform, 0.2f, 3f, 0.4f, 1, 0.4f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    case 1:
                        Debug.Log("Take Damage Up");
                        InflictDmgAndKnockbackv2(transform, 0.3f, 5f, 0.5f, 1, 0.4f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    case 2:
                        Debug.Log("Take Damage Up");
                        InflictDmgAndKnockbackv2(transform, 1f, 10f, 1f, 1, 1f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[0].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    default:
                        break;
                }

                Debug.Log(nbOfCombo);
                break;
            //Coups milieux
            case 1:
                switch (nbOfCombo)
                {
                    case 0:
                        Debug.Log("Take Damage Middle");
                        InflictDmgAndKnockbackv2(transform, 0.5f, 5f, 0.1f, -1, 0.4f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    case 1:
                        Debug.Log("Take Damage Middle");
                        InflictDmgAndKnockbackv2(transform, 0.2f, 1f, 0.1f, 1, 0.4f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    case 2:
                        Debug.Log("Take Damage Middle");
                        InflictDmgAndKnockbackv2(transform, 0.7f, 5f, 1f, 1, 1f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[1].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    default:
                        break;
                }

                Debug.Log(nbOfCombo);
                break;
            case 2:
                switch (nbOfCombo)
                {
                    //Coup bas
                    case 0:
                        Debug.Log("Take Damage Down");
                        InflictDmgAndKnockbackv2(transform, 0.2f, 1f, 0.1f, 1, 0.4f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    case 1:
                        InflictDmgAndKnockbackv2(transform, 0.2f, 1f, 0.1f, 1, 0.4f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].z), 1f);
                        Debug.Log("Take Damage Down");
                        break;
                    case 2:
                        Debug.Log("Take Damage Down");
                        InflictDmgAndKnockbackv2(transform, 0.5f, 5f, 0.1f, -1, 1f);
                        MonsterMove(
                            new Vector3(listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].x,
                                listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].y,
                                listOfMovementEachAttack[2].listOfMoveVector[nbOfCombo].z), 1f);
                        break;
                    default:
                        break;
                }

                Debug.Log(nbOfCombo);
                break;
            default:
                break;
        }
    }


    private void OnDrawGizmosSelected()
    {
        if (hitPoints == null)
        {
            return;
        }
        foreach (var point in hitPoints)
        {
            Gizmos.DrawWireSphere(point.position, attackRange);
        }
    }

    //Dernier coup qui met fin au combo, parametre permettant de donner plus de temps avant que le joueur puisse refrapper (équilibrage).
    private void FinishCombo(float cooldownAttack)
    {
        timeBeforeAttacking = cooldownAttack;
    }

    //Fonction permettant d'infliger des dégats 
    private void InflictDmgAndKnockbackv2(Transform transformHitPoint, float durationKnockback, float knockBackPower,
        float knockBackModifier, float yDirection, float timeBeforeAttackingAgain)
    {
        Collider2D hitEnnemy = Physics2D.OverlapCircle(transformHitPoint.position, attackRange, enemyLayers);

        if (hitEnnemy && hitEnnemy.GetComponent<PlayerMovement>().player != playerMoves.player &&
            !hitEnnemy.GetComponent<CombatStat>().isInvincible)
        {
            // CombatStat
            CombatStat hitEnnemyCombatStat = hitEnnemy.GetComponent<CombatStat>();

            //Si l'ennemi n'est pas invincible 
            if (!hitEnnemyCombatStat.isInvincible)
            {
                //Application du knockback
                if (hitEnnemy.GetComponent<Guard>().guard)
                {
                    Debug.Log("Est en garde");
                    if (playerMoves.isFliped &&
                        hitEnnemy.GetComponent<PlayerMovement>().isFliped)
                    {
                        Debug.Log("dos");
                        hitEnnemyCombatStat.TakeDmg(hitEnnemyCombatStat.attack);
                    }
                    else if (!playerMoves.isFliped &&
                             !hitEnnemy.GetComponent<PlayerMovement>().isFliped)
                    {
                        Debug.Log("dos");
                        hitEnnemyCombatStat.TakeDmg(hitEnnemyCombatStat.attack);
                    }
                    else // si le joueur frapper ne tourne pas le dos a son attaquant il bloque une partie des dégat infliger
                    {
                        Debug.Log("block");
                        hitEnnemyCombatStat.TakeDmgGard(hitEnnemyCombatStat.attack);
                    }
                }

                if (!hitEnnemy.GetComponent<Guard>().guard)// si le joueur ne garde pas il prend des dégat normaux et est repousser en arrière
                {
                    Debug.Log("N'est pas en garde");
                    hitEnnemy.GetComponent<PlayerMovement>().AddKnockBack(durationKnockback, knockBackPower,
                        knockBackModifier, transform.position, yDirection);

                    hitEnnemyCombatStat.TakeDmg(hitEnnemyCombatStat.attack);
                }


                // Si le monstre attaquant est de type poison
                if (playerStats.powerPoisonPlayer > 0)
                {
                    Debug.Log("Poison");
                    hitEnnemyCombatStat.TakeDotPoison(playerStats.powerPoisonPlayer);
                }

                // Si le monstre attaquant est de type saignement
                if (playerStats.powerBloodPlayer > 0)
                {
                    Debug.Log("Blood");
                    hitEnnemyCombatStat.TakeDotBlood(playerStats.powerBloodPlayer);
                }
            }
            else
            {
                //Si l'ennemi est invincible, il ne subit pas de dégats et il ne prend pas de knockback
                Debug.Log("Esquive de " + gameObject.name);
            }
        }

        //Ennemi ou non, le temps avant de faire une attaque se met en place
        timeBeforeAttacking = timeBeforeAttackingAgain;
    }

    //Fonction qui active le lerp qui va faire bouger le monstre
    private void MonsterMove(Vector3 addMovement, float speed)
    {
        //permet de faire un lerp uniquement si le monstre est sur le sol
        if (playerMoves.isGrounded)
        {
            onLerp = true;
            timerOnLerp = resetTimerOnLerp;
            if (playerMoves.isFliped)
            {
                nextMove = transform.position - addMovement;
            }
            else
            {
                nextMove = transform.position + addMovement;
            }

            speedMove = speed;
        }
    }
}