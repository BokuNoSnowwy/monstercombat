﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public string player;

    public float moveHorizontal;
    public float moveVertical;

    public float rayDistance = 2.5f;

    public float jumpForce = 10f;
    public float fallMultiplier = 1f;
    public float baseFallMultiplier = 1f;
    public float highFallMultiplier = 5f;

    public float lowJumpMultiplier = 2f;

    public bool isFliped = false;

    public LayerMask layers;
    public LayerMask layerArena;

    //private LayerMask playerLayerValue;
    public LayerMask principalGroundLayers;

    public bool isGrounded = false;

    // si le player touche le rez de chaussé
    public bool isPrincipalGrounded;
    public Rigidbody2D rgbd;


    //Dash/Run

    public bool canDash = true;
    public bool isDashing;

    public bool isRunning;
    public bool continueRunning;
    public bool betweenRun;

    public float speed = 7f;
    public float speedRun;
    public float speedBase;

    public float timerForDashRun = 0.5f;
    public float resetTimer = 0.5f;

    public float timerBetweenRun;
    public float resetTimerBetween = 0.25f;
    public int counterInput;

    public float distanceDash = 2.5f;
    public float normalDistanceDash = 2.5f;

    //KnockBack
    public float knockback;
    private float knockbackLenght;
    public float knockbackCount = 0;
    public bool knockbackRight;
    public bool knockbackUp;
    public float highKnockbackModifier;

    private PlayerAttack _playerAttack;


    // Start is called before the first frame update
    void Start()
    {
        rgbd = gameObject.GetComponent<Rigidbody2D>();
        _playerAttack = GetComponent<PlayerAttack>();
        //playerLayerValue = LayerMask.NameToLayer("PlayerLayer");
    }

    // Update is called once per frame
    void Update()
    {
        if (knockbackCount <= 0)
        {
            if (!isDashing)
            {
                //SI une touche est pressée 
                if (Input.anyKey)
                {
                    //Récupère l'axe de 0 à 1 de la pression du bouton ou du joystick
                    moveHorizontal = Input.GetAxis("Horizontal_" + player);
                    //Multiplication par le temps pour que le joueur se déplace à une vitesse normale
                    moveHorizontal *= Time.deltaTime;

                    //Récupère l'axe de 0 à 1 de la pression du bouton ou du joystick
                    moveVertical = Input.GetAxis("Vertical_" + player);
                    //Multiplication par le temps pour que le joueur se déplace à une vitesse normale
                    moveVertical *= Time.deltaTime;
                    
                    
                    if (!_playerAttack.onCombo)
                    {
                        //Fait bouger le personnage dans l'espace avec une vitesse donnée, si il va a gauche
                        //change la direction du sprite et inversement.
                        //Modifie aussi la valeur isFliped pour gérer la direction de l'épée
                        if (moveHorizontal < 0 && !GetComponent<Guard>().guard) //check si la garde est activé pour empecher le joueur de bouger
                        {
                            SideLeft();
                        }
                        else if (moveHorizontal > 0 && !GetComponent<Guard>().guard) //check si la garde est activé pour empecher le joueur de bouger
                        {
                            SideRight();
                        }
                    }
                }
                else
                {
                    //Si le joueur ne touche a aucune touche, remet à 0 sa vitesse.
                    rgbd.velocity = new Vector2(0f, rgbd.velocity.y);
                }

                //Gestion de la detection du mur pour le dodge
                //Verification de ou le joueur fait face
                if (isFliped)
                {
                    //Si le joueur regarde a gauche, créer un raycast a gauche qui va detecter si il y a un mur proche. Si oui, réduit la distance du dash pour eviter qu'il rentre dans le mur
                    RaycastHit2D hitWallLeft = Physics2D.Raycast(
                        new Vector2(transform.position.x - 0.25f, transform.position.y - 0.5f), Vector2.left,
                        distanceDash,
                        layerArena);
                    Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.5f), Vector2.left,
                        Color.blue, distanceDash);

                    if (hitWallLeft)
                    {
                        float distance = hitWallLeft.distance;
                        if (distance < 0)
                        {
                            distanceDash = distance * -1;
                        }
                        else
                        {
                            distanceDash = distance;
                        }
                    }
                    else
                    {
                        //Si il ne detecte pas de mur, reset la distance du dash (ici 2.5f)
                        distanceDash = normalDistanceDash;
                    }
                }
                else
                {
                    //Si le joueur regarde a droite, créer un raycast a droite qui va detecter si il y a un mur proche. Si oui, réduit la distance du dash pour eviter qu'il rentre dans le mur
                    RaycastHit2D hitWallRight = Physics2D.Raycast(
                        new Vector2(transform.position.x + 0.25f, transform.position.y - 0.5f), Vector2.right,
                        distanceDash,
                        layerArena);

                    Debug.DrawRay(new Vector2(transform.position.x, transform.position.y - 0.5f), Vector2.right,
                        Color.red, distanceDash);

                    if (hitWallRight)
                    {
                        float distance = hitWallRight.distance;
                        if (distance < 0)
                        {
                            distanceDash = distance * -1;
                        }
                        else
                        {
                            distanceDash = distance;
                        }
                    }
                    else
                    {
                        //Si il ne detecte pas de mur, reset la distance du dash (ici 2.5f)
                        distanceDash = normalDistanceDash;
                    }
                }

                //Detection du sol, detecte si le raycast sortant du joueur touche un layer spécifique
                if (Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center,
                    GetComponent<BoxCollider2D>().bounds.extents, 0f, Vector2.down, rayDistance, layers))
                {
                    //Si oui, le joueur peut sauter car il est dans l'état isGrounded
                    isGrounded = true;
                }
                else
                {
                    isGrounded = false;
                }


                //Detection du sol principal tout en bas, detecte si le raycast sortant du joueur touche un layer spécifique
                if (Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center,
                    GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.down, rayDistance, principalGroundLayers))
                {
                    isPrincipalGrounded = true;
                }
                else
                {
                    isPrincipalGrounded = false;
                }


                if (canDash)
                {
                    //Si la combinaison d'input Dodge + movement Horizontal est fait, fait un dash dans la direction du move Horizontal
                    if (Input.GetAxis("Horizontal_" + player) > 0 && Input.GetButtonDown("Dodge_" + player))
                    {
                        if (isGrounded && canDash || isPrincipalGrounded && canDash)
                        {
                            StartCoroutine(DodgeEnumGround(moveHorizontal));
                        }
                    }
                    else if (Input.GetAxis("Horizontal_" + player) < 0 && Input.GetButtonDown("Dodge_" + player))
                    {
                        if (isGrounded && canDash || isPrincipalGrounded && canDash)
                        {
                            StartCoroutine(DodgeEnumGround(moveHorizontal));
                        }
                    }
                }
            }

            //Detection du sol principal tout en bas, detecte si le raycast sortant du joueur touche un layer spécifique
            if (Physics2D.BoxCast(GetComponent<BoxCollider2D>().bounds.center,
                GetComponent<BoxCollider2D>().bounds.size, 0f, Vector2.down, rayDistance, principalGroundLayers))
            {
                isPrincipalGrounded = true;
            }
            else
            {
                isPrincipalGrounded = false;
            }

            //Si le joueur est en train de tomber 
            if (rgbd.velocity.y < 0)
            {
                //Ajoute une valeur de gravité dans la chute du joueur et permet de faire une chute plus ou moins longue avec le fallMultiplier + la gravité du Rigidbody
                rgbd.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier) * Time.deltaTime;
            }
            //Si le joueur est en l'air et qu'on ne touche pas le bouton de saut
            else if (rgbd.velocity.y > 0 && !Input.GetButton("JumpPlatformer_" + player))
            {
                //Permet de faire un petit saut ou un saut plus grand si on reste appuyé sur la barre de saut, la vitesse de la chute (lowJumpMultiplier) est moins importante
                //que le fallMultiplier pour donner un meilleur effet de chute lors d'un petit saut
                rgbd.velocity += Vector2.up * (Physics2D.gravity.y * (lowJumpMultiplier) * (Time.deltaTime * 1.3f));
            }


            if (isGrounded || isPrincipalGrounded)
            {
                fallMultiplier = baseFallMultiplier;
                if (moveVertical >= 0 && Input.GetButton("JumpPlatformer_" + player) && !GetComponent<Guard>().guard) // vérifie si le joueur garde et si il est au sol ainsi que l'input jump
                {
                    Jump();
                }
            }

            PlayerRun();

            //Reset le knockback
            ResetKnockBack();
        }
        else
        {
            //Fait un knockback en fonction de la direction et de où le knockback part
            if (knockbackRight)
            {
                if (knockbackUp)
                {
                    rgbd.velocity = new Vector2(knockback, knockback * highKnockbackModifier);
                }
                else
                {
                    rgbd.velocity = new Vector2(knockback, -knockback * highKnockbackModifier);
                }
            }
            else
            {
                if (knockbackUp)
                {
                    rgbd.velocity = new Vector2(-knockback, knockback * highKnockbackModifier);
                }
                else
                {
                    rgbd.velocity = new Vector2(-knockback, -knockback * highKnockbackModifier);
                }
            }

            knockbackCount -= Time.deltaTime;
            knockback -= knockback * 0.1f;
        }
    }

    //Fonction permettant de sauter
    public void Jump()
    {
        rgbd.velocity = Vector2.up * jumpForce;
        ResetRunning();
    }

    private void PlayerRun()
    {
        if (isGrounded || isPrincipalGrounded)
        {
            //Check Run
            //Si le joueur appuis sur une des touches de déplacements,
            if (!isRunning)
            {
                if (Input.GetButtonUp("Horizontal_" + player))
                {
                    counterInput = 1;
                }
            }

            //Il a 0.25s pour réappuyer sur un des touches de déplacement et courir
            if (counterInput >= 1)
            {
                timerForDashRun -= Time.deltaTime;
                isRunning = true;
                if (timerForDashRun <= 0)
                {
                    timerForDashRun = resetTimer;
                    counterInput = 0;
                    isRunning = false;
                }
            }

            //Si le joueur a appuyé 2 fois sur la touche de déplacement et qu'il reste appuyé sur le bouton de déplacement, active le booleen continueRunning
            //qui augmente la vitesse de base du joueur
            if (isRunning)
            {
                if (Input.GetButtonDown("Horizontal_" + player))
                {
                    continueRunning = true;
                }
            }

            //Augemente la vitesse du joueur si il court, la remet comme avant si il ne court pas
            if (continueRunning)
            {
                if (Input.GetButtonDown("Horizontal_" + player))
                {
                    speed = speedRun;
                }

                if (Input.GetButtonUp("Horizontal_" + player))
                {
                    betweenRun = true;
                }
            }
            else
            {
                speed = speedBase;
            }

            //Le joueur a 0.25s pour changer de direction durant sa course, sinon il perd sa vitesse
            if (betweenRun)
            {
                if (!(Input.GetButton("Horizontal_" + player)))
                {
                    timerBetweenRun -= Time.deltaTime;
                }

                if (timerBetweenRun <= 0)
                {
                    continueRunning = false;
                    timerBetweenRun = resetTimerBetween;
                    betweenRun = false;
                }
            }
        }
    }

    //Fonction permettant au joueur de faire une esquive sur le coté, la direction de l'esquive dépend de l'input gauche ou droite
    public IEnumerator DodgeEnumGround(float moveHorizontal)
    {
        canDash = false;
        isDashing = true;
        //Disable l'attaque
        _playerAttack.canAttack = false;

        //Désactive les colliders pour pouvoir traverser le joueur ennemi 
        GetComponent<Collider2D>().enabled = false;

        StartCoroutine(MassModifyDodge());

        if (moveHorizontal <= 0)
        {
            transform.DOMove(
                    new Vector3(transform.position.x - distanceDash, transform.position.y, transform.position.z), 0.51f)
                .SetEase(Ease.Linear);
        }
        else
        {
            transform.DOMove(
                    new Vector3(transform.position.x + distanceDash, transform.position.y, transform.position.z), 0.51f)
                .SetEase(Ease.Linear);
        }

        yield return new WaitForSeconds(0.5f);
        //Réactive les colliders
        GetComponent<Collider2D>().enabled = true;
        StartCoroutine(TimeBetweenDodges());
        isDashing = false;
        //Réactive l'attaque
        _playerAttack.canAttack = true;
        //Reset les combos
        _playerAttack.ResetComboTimer();
    }

    //Change la mass du rigidbody pendant le temps du dodge
    //Permet au joueur A de ne pas affecter la position du joueur B quand il fait un dodge et ratteris sur le joueur B
    public IEnumerator MassModifyDodge()
    {
        GetComponent<Rigidbody2D>().mass = 0.01f;
        yield return new WaitForSeconds(0.6f);
        GetComponent<Rigidbody2D>().mass = 1f;
    }

    //Timer avec indication visuel d'attente d'esquive, en effet le joueur ne peut pas faire plein d'esquive d'affilé
    public IEnumerator TimeBetweenDodges()
    {
        Color color = new Color(0.7f, 0.7f, 0.7f);
        GetComponent<SpriteRenderer>().DOColor(color, 0.25f)
            .OnComplete(() => GetComponent<SpriteRenderer>().DOColor(Color.white, 0.25f)
                .OnComplete(() => GetComponent<SpriteRenderer>().DOColor(color, 0.25f)
                    .OnComplete(() => GetComponent<SpriteRenderer>().DOColor(Color.white, 0.25f))));
        yield return new WaitForSeconds(1f);
        canDash = true;
    }

    //Faire regarder le joueur vers la gauche
    public void SideLeft()
    {
        isFliped = true;
        transform.eulerAngles = new Vector3(0f, -180f, 0f);
        transform.Translate(-moveHorizontal * speed, 0, 0);
    }

    //Faire regarder le joueur vers la droite
    public void SideRight()
    {
        isFliped = false;
        transform.eulerAngles = new Vector3(0f, 0f, 0f);
        transform.Translate(moveHorizontal * speed, 0, 0);
    }

    public void ResetRunning()
    {
        continueRunning = false;
        betweenRun = false;
        isRunning = false;
        counterInput = 0;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("headMonster"))
        {
            Debug.Log("TouchedPlayer");

            GameObject enemyPlayer = other.gameObject;
            if (enemyPlayer.transform.position.x <= transform.position.x)
            {
                rgbd.AddForce(Vector2.right, ForceMode2D.Impulse);
            }
            else
            {
                rgbd.AddForce(Vector2.left, ForceMode2D.Impulse);
            }
        }
    }

    //Quand la fonction est appellée, on attribue les valeurs pour le knockback que le joueur va subir
    //Durée du knockback
    //Puissance du knockback
    //Modifier pour que le knockback vers le haut puisse etre regler
    //Ennemy pour le check de la direction où repousser le joueur
    //Direction pour savoir si le knock back sera vers le haut ou vers le bas (on récupère la valeur Y (negatif ou positif))
    public void AddKnockBack(float durationKnockBack, float knockBackPower, float knockBackModifier,
        Vector3 ennemy, float yDirection)
    {
        knockbackCount = durationKnockBack;
        knockback = knockBackPower;
        highKnockbackModifier = knockBackModifier;

        //Empèche le joueur de frapper quand il se fait prapper lui meme (quand il se prend un knockback)
        GetComponent<PlayerAttack>().canAttack = false;

        if (yDirection > 0)
        {
            knockbackUp = true;
        }
        else if (yDirection < 0)
        {
            knockbackUp = false;
        }

        if (ennemy.x < transform.position.x)
        {
            knockbackRight = true;
        }
        else
        {
            knockbackRight = false;
        }
    }

    public void ResetKnockBack()
    {
        //Reset du knockback
        knockback = 0;
        knockbackCount = 0;
        highKnockbackModifier = 0;
        GetComponent<PlayerAttack>().canAttack = true;
    }
}