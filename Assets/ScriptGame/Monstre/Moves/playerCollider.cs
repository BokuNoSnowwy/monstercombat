﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerCollider : MonoBehaviour
{
    public float ejection = 1;
    public LayerMask layers;
    public Rigidbody2D rgbd;
    public float distance = 1f;

    private PlayerMovement player;
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        player = GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {// Permet d'empecher le player de rester coincer dans les murs
        
        RaycastHit2D wallrightInfo = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y ), Vector2.right, distance, layers);
        RaycastHit2D WallleftInfo = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.left, distance, layers);

        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y ), Vector2.right, Color.red, distance);
        Debug.DrawRay(new Vector2(transform.position.x, transform.position.y ), Vector2.left, Color.blue, distance);
        
        if (wallrightInfo == true && player.moveHorizontal <= 0 )
        {
            Debug.Log("HitLeft");
            rgbd.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            rgbd.velocity += Vector2.left * ejection * Time.deltaTime;
        }

        else if (WallleftInfo == true && player.moveHorizontal > 0)
        {
            Debug.Log("HitRight");
            rgbd.velocity += Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            rgbd.velocity += Vector2.right * ejection * Time.deltaTime;

        }
    }
}
