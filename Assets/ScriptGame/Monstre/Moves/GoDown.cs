﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoDown : MonoBehaviour
{
    private Rigidbody2D rb2D;

    // Distance du rayon
    public float rayDistance = 1f;

    private PlayerMovement playerMovement;

    // Layer
    private LayerMask playerLayerValue;

    // Bool
    private bool coroutineIsRunning;

    // Start is called before the first frame update
    void Start()
    {
        playerLayerValue = LayerMask.NameToLayer("PlayerLayer");
        rb2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        playerMovement = GetComponent<PlayerMovement>();

        PlayerGoDown();
    }

    private void PlayerGoDown()
    {
        if (playerMovement.player.Equals("P1"))
        {
            playerLayerValue = LayerMask.NameToLayer("PlayerLayer1");
        }
        else if (playerMovement.player.Equals("P2"))
        {
            playerLayerValue = LayerMask.NameToLayer("PlayerLayer2");
        }

        if (playerMovement.isGrounded && !playerMovement.isPrincipalGrounded)
        {
            // Si l'on appuie sur bas + sauter
            if (playerMovement.moveVertical < 0 && Input.GetButtonDown("JumpPlatformer_" + playerMovement.player))
            {
                // Faire descendre le joueur de la plateforme
                StartCoroutine("JumpOff");
            }
        }

        if (rb2D.velocity.y > 0 )
        {
            Physics2D.IgnoreLayerCollision(playerLayerValue, LayerMask.NameToLayer("Ground"), true);
        }
        else if (rb2D.velocity.y <= 0 && !coroutineIsRunning)
        {
            Physics2D.IgnoreLayerCollision(playerLayerValue, LayerMask.NameToLayer("Ground"), false);
        }
    }

    IEnumerator JumpOff()
    {
        coroutineIsRunning = true;
        Physics2D.IgnoreLayerCollision(playerLayerValue, LayerMask.NameToLayer("Ground"), true);
        yield return new WaitForSeconds(0.4f);
        Physics2D.IgnoreLayerCollision(playerLayerValue, LayerMask.NameToLayer("Ground"), false);
        coroutineIsRunning = false;
    }
}