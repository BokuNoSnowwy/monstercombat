﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CombatStat : MonoBehaviour
{
    [SerializeField]
    public float health, maxHealth, defense, attack , invincibility ,powerPoisonPlayer, powerBloodPlayer, DOTPoisonEnemy , DOTBleedEnemy;   //toutes les stats du joueurs
    public bool isDodging , onPoison , onBlood;     //bool de gestion stat
    public Slider barHealth;
    public PlayerMovement playerMovement;

    public float cooldownPoison;
    public float cooldownBleeding;
    
    public float resetCooldownPoison = 1f;
    public float resetCooldownBleeding = 1f;

    public int nbOfTickPoison = 0;
    public int nbOfTickBleed = 0;
        

    public bool isInvincible;
    public float timeInvincible;

    private void Start()
    {
        playerMovement = GetComponent<PlayerMovement>();
        //Récupère les barres de vies sur la scène.
        if (!barHealth)
        {
            if (playerMovement.player == "P1") 
            {
                barHealth = GameObject.FindGameObjectWithTag("healthbar1").GetComponent<Slider>();
            }
            else if (playerMovement.player == "P2")
            {
                barHealth = GameObject.FindGameObjectWithTag("healthbar2").GetComponent<Slider>();
            }
        }
    }

    private void Update()
    {
        // Le monstre est invincible pendant le dash
        isInvincible = playerMovement.isDashing;
        
        if (isInvincible)
        {
            timeInvincible -= Time.deltaTime;
            if (timeInvincible <= 0)
            {
                timeInvincible = 0;
                isInvincible = false;
            }
        }
        
        
        barHealth.value = CalculateHealth();
        
        if (health <= 0) //si pu de pv monstre disparait donc mort
        {
            gameObject.SetActive(false);
        }
        
        //Inflige du dégats de poison chaque seconde pendant 3 secondes
        if (onPoison)
        {
            if (nbOfTickPoison <= 3)
            {
                cooldownPoison -= Time.deltaTime;
                if (cooldownPoison <= 0)
                {
                    health -= DOTPoisonEnemy;
                    nbOfTickPoison += 1;
                    cooldownPoison = resetCooldownPoison;
                }
            }
            else
            {
                onPoison = false;
            }
        }
        
        //Inflige du dégats de saignement chaque seconde pendant 3 secondes
        if (onBlood)
        {
            if (nbOfTickBleed <= 3)
            {
                cooldownBleeding -= Time.deltaTime;
                if (cooldownBleeding <= 0)
                {
                    float dmg = 1 - (100 / (100 + defense));
                    health -= DOTBleedEnemy * dmg *0.5f;
                    nbOfTickBleed += 1;
                    cooldownBleeding = resetCooldownBleeding;
                }
            }
            else
            {
                onBlood = false;
            }
        }
    }
    //Inflige des dégats, ces derniers sont mitigés avec l'armure
    public void TakeDmg(float attack)
    {
        float dmgperred = 0; //reduction du pourcentage de dégat reçu en fonction de la defense
        if (defense >= 0)
        {
            dmgperred = 1 - (100 / (100 + defense));
            Debug.Log(attack * dmgperred);
            health -= attack * dmgperred;
        }
    }
    
    //Inflige des dégats, mitigés avec l'armure et divisé par 2 a cause de la garde
    public void TakeDmgGard(float attack)
    {
        float dmg = 0;
        if (defense >= 0)
        {
            dmg = 1 - (100 / (100 + defense));
            Debug.Log(attack * dmg);
            health -= ((attack * dmg)/ 4);
        }
    }
    //Inflige un dot de poison 
    public void TakeDotPoison(float attackPoison)
    {
        onPoison = true;
        nbOfTickPoison = 0;
        DOTPoisonEnemy = attackPoison;
    }
    //Inflige un dot de saignement
    public void TakeDotBlood(float attackBlood)
    {
        onBlood = true;
        nbOfTickBleed = 0;
        DOTBleedEnemy = attackBlood;
    }
    float CalculateHealth() //calcule la value du slider
    {
        return health / maxHealth;
    }

    //Rend le joueur invincible 
    public void MakeInvicible(float time)
    {
        timeInvincible = time;
    }

    //Reset les valeurs de vie du respawn
    public void respawn()
    {
        onBlood = false;
        onPoison = false;
        health = maxHealth;
    }

    //statistique de base des monstres lorsque l'on arrive sur la scene arene pour supprimer effet des equipement précédent
    public void resetStatMonstre1()
    {
        maxHealth = 150;
        health = 150;
        defense = 75;
        attack = 25;
        powerBloodPlayer = 0;
        powerPoisonPlayer = 0;
    }
    public void resetStatMonstre2()
    {
        maxHealth = 200;
        health = 200;
        defense = 50;
        attack = 20;
        powerBloodPlayer = 0;
        powerPoisonPlayer = 0; 
    }
    public void resetStatMonstre3()
    {
        maxHealth = 200;
        health = 200;
        defense = 25;
        attack = 15;
        powerBloodPlayer = 0;
        powerPoisonPlayer = 5;
    }
    public void resetStatMonstre4()
    {
        maxHealth = 200;
        health = 200;
        defense = 75;
        attack = 15;
        powerBloodPlayer = 5;
        powerPoisonPlayer = 0;  
    }

    //statistique des equipement qui sont appliqué au stat du personnage
    public void equipement0()
    {
        maxHealth += 30;
        health += 30;
        defense += 7;
        powerPoisonPlayer += 6;
    }
    public void equipement1()
    {
        attack += 6;
        maxHealth += 40;
        health += 40;
        powerPoisonPlayer += 3;
    }
    public void equipement2()
    {
        attack += 10;
        maxHealth += 20;
        health += 20;
        defense += 8;
        powerBloodPlayer += 3;
    }
    public void equipement3()
    {
        attack -= 15;
        defense += 40;
        maxHealth += 40;
        health += 40;
    }
    public void equipement4()
    {
        maxHealth += 30;
        health += 30;
        attack += 15;
        defense -= 20;
    }
    public void equipement5()
    {
        maxHealth += 10;
        health += 10;
        attack += 16;
        defense -= 5;
        
    }
    public void equipement6()
    {
        maxHealth -= 40;
        health -= 40;
        attack += 30;
        defense -= 18;
        
    }
    public void equipement7()
    {
        attack += 15;
        defense += 10;
        powerPoisonPlayer += 13;
    }
    public void equipement8()
    {
        maxHealth += 40;
        health += 40;
        defense += 7;
        powerPoisonPlayer += 9;
    }
    public void equipement9()
    {
        maxHealth -= 40;
        health -= 40;
        attack += 16;
        powerPoisonPlayer += 2;
    }
    public void equipement10()
    {
        maxHealth += 32;
        health += 32;
        attack += 13;
        defense -= 6;
        powerPoisonPlayer += 3;
    }
    public void equipement11()
    {
        attack += 25;
        defense += 35;
    }
}
