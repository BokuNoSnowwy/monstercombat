﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class equipementArene : MonoBehaviour
{
    public GameObject recupInfoEquipement;
    public GameObject recupInfoPerso;
    public GameObject areneManager;
    public GameObject monstre1;
    public GameObject monstre2;
    
    public int heroP1;
    public int heroP2;
    // Start is called before the first frame update
    void Start()
    {
        recupInfoPerso = GameObject.Find("Data");
        areneManager = GameObject.Find("MonsterManager");
        monstre1 = areneManager.GetComponent<MonsterLinkPlayerManager>().monstre1;
        monstre2 = areneManager.GetComponent<MonsterLinkPlayerManager>().monstre2;
        recupInfoEquipement = GameObject.Find("SaveObjectSceneArena");
        heroP1 = recupInfoPerso.GetComponent<Info_Sauvegarder>().p1;
        heroP2 = recupInfoPerso.GetComponent<Info_Sauvegarder>().p2;
        //reset des statisque de chaque monstre avec un appel des void de combatStat
        if (heroP1 == 0)
        {
            monstre1.GetComponent<CombatStat>().resetStatMonstre1();
        }
        else if (heroP1 == 1)
        {
            monstre1.GetComponent<CombatStat>().resetStatMonstre2();
        }
        else if (heroP1 == 2)
        {
            monstre1.GetComponent<CombatStat>().resetStatMonstre3();
        }
        else if (heroP1 == 3)
        {
            monstre1.GetComponent<CombatStat>().resetStatMonstre4();
        }
        if (heroP2 == 0)
        {
            monstre2.GetComponent<CombatStat>().resetStatMonstre1();
        }
        else if (heroP2 == 1)
        {
            monstre2.GetComponent<CombatStat>().resetStatMonstre2();
        }
        else if (heroP2 == 2)
        {
            monstre2.GetComponent<CombatStat>().resetStatMonstre3();
        }
        else if (heroP2 == 3)
        {
            monstre2.GetComponent<CombatStat>().resetStatMonstre4();
        }
        //check les equipement sélectionner par les joueurs et on appel les void pour appliquer les buff.
        for (int i = 0; i < 11; i++)
        {
            if (recupInfoEquipement.GetComponent<SaveObjectPlayer>().FirstChoiceP1 == i)
            {
                if (i == 0)
                {
                    monstre1.GetComponent<CombatStat>().equipement0();
                }
                if (i == 1)
                {
                    monstre1.GetComponent<CombatStat>().equipement1();
                }
                if (i == 2)
                {
                    monstre1.GetComponent<CombatStat>().equipement2(); 
                }
                if (i == 3)
                {
                    monstre1.GetComponent<CombatStat>().equipement3();  
                }
                if (i == 4)
                {
                    monstre1.GetComponent<CombatStat>().equipement4();
                }
                if (i == 5)
                {
                    monstre1.GetComponent<CombatStat>().equipement5();
                }
                if (i == 6)
                {
                    monstre1.GetComponent<CombatStat>().equipement6();
                }
                if (i == 7)
                {
                    monstre1.GetComponent<CombatStat>().equipement7();
                }
                if (i == 8)
                {
                    monstre1.GetComponent<CombatStat>().equipement8();
                }
                if (i == 9)
                {
                    monstre1.GetComponent<CombatStat>().equipement9();
                }
                if (i == 10)
                {
                    monstre1.GetComponent<CombatStat>().equipement10();
                }
                if (i == 11)
                {
                    monstre1.GetComponent<CombatStat>().equipement11();
                }
            }
            else if (recupInfoEquipement.GetComponent<SaveObjectPlayer>().SecondChoiceP1 == i)
            {
                if (i == 0)
                {
                    monstre1.GetComponent<CombatStat>().equipement0();
                }
                if (i == 1)
                {
                    monstre1.GetComponent<CombatStat>().equipement1();
                }
                if (i == 2)
                {
                    monstre1.GetComponent<CombatStat>().equipement2(); 
                }
                if (i == 3)
                {
                    monstre1.GetComponent<CombatStat>().equipement3();  
                }
                if (i == 4)
                {
                    monstre1.GetComponent<CombatStat>().equipement4();
                }
                if (i == 5)
                {
                    monstre1.GetComponent<CombatStat>().equipement5();
                }
                if (i == 6)
                {
                    monstre1.GetComponent<CombatStat>().equipement6();
                }
                if (i == 7)
                {
                    monstre1.GetComponent<CombatStat>().equipement7();
                }
                if (i == 8)
                {
                    monstre1.GetComponent<CombatStat>().equipement8();
                }
                if (i == 9)
                {
                    monstre1.GetComponent<CombatStat>().equipement9();
                }
                if (i == 10)
                {
                    monstre1.GetComponent<CombatStat>().equipement10();
                }
                if (i == 11)
                {
                    monstre1.GetComponent<CombatStat>().equipement11();
                }
            }
            else if (recupInfoEquipement.GetComponent<SaveObjectPlayer>().FirstChoiceP2 == i)
            {
                if (i == 0)
                {
                    monstre2.GetComponent<CombatStat>().equipement0();
                }
                if (i == 1)
                {
                    monstre2.GetComponent<CombatStat>().equipement1();
                }
                if (i == 2)
                {
                    monstre2.GetComponent<CombatStat>().equipement2(); 
                }
                if (i == 3)
                {
                    monstre2.GetComponent<CombatStat>().equipement3();  
                }
                if (i == 4)
                {
                    monstre2.GetComponent<CombatStat>().equipement4();
                }
                if (i == 5)
                {
                    monstre2.GetComponent<CombatStat>().equipement5();
                }
                if (i == 6)
                {
                    monstre2.GetComponent<CombatStat>().equipement6();
                }
                if (i == 7)
                {
                    monstre2.GetComponent<CombatStat>().equipement7();
                }
                if (i == 8)
                {
                    monstre2.GetComponent<CombatStat>().equipement8();
                }
                if (i == 9)
                {
                    monstre2.GetComponent<CombatStat>().equipement9();
                }
                if (i == 10)
                {
                    monstre2.GetComponent<CombatStat>().equipement10();
                }
                if (i == 11)
                {
                    monstre2.GetComponent<CombatStat>().equipement11();
                }
            }
            else if (recupInfoEquipement.GetComponent<SaveObjectPlayer>().SecondChoiceP2 == i)
            {
                if (i == 0)
                {
                    monstre2.GetComponent<CombatStat>().equipement0();
                }
                if (i == 1)
                {
                    monstre2.GetComponent<CombatStat>().equipement1();
                }
                if (i == 2)
                {
                    monstre2.GetComponent<CombatStat>().equipement2(); 
                }
                if (i == 3)
                {
                    monstre2.GetComponent<CombatStat>().equipement3();  
                }
                if (i == 4)
                {
                    monstre2.GetComponent<CombatStat>().equipement4();
                }
                if (i == 5)
                {
                    monstre2.GetComponent<CombatStat>().equipement5();
                }
                if (i == 6)
                {
                    monstre2.GetComponent<CombatStat>().equipement6();
                }
                if (i == 7)
                {
                    monstre2.GetComponent<CombatStat>().equipement7();
                }
                if (i == 8)
                {
                    monstre2.GetComponent<CombatStat>().equipement8();
                }
                if (i == 9)
                {
                    monstre2.GetComponent<CombatStat>().equipement9();
                }
                if (i == 10)
                {
                    monstre2.GetComponent<CombatStat>().equipement10();
                }
                if (i == 11)
                {
                    monstre2.GetComponent<CombatStat>().equipement11();
                }
            }
        }
    }
}
