﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausedMenu : MonoBehaviour
{
    public bool GameIsPaused = false;
    public GameObject pauseMenuUI;
    public string retryscene;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) //si tu appuis sur echap alors ça va affiche puis enlever si tu rappuie le menu
        {
            if (GameIsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume() //remet en route le jeu et cache le menu UI
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }
    public void Pause() //met en pause le jeu et affiche l ui menu
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
    public void LoadScene(int scene) //permet de changer de scene et remettre le temps
    {
        SceneManager.LoadScene(scene);
        Time.timeScale = 1f;
    }
    public void Quit() //permet de quitter l'application
    {
        Application.Quit();
    }
}
